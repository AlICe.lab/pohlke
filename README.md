<!--
SPDX-FileCopyrightText: 2021-2024 Julien Rippinger, Michel Lefèvre <alicelab.be>

SPDX-License-Identifier: CC-BY-4.0
-->

# Pohlke - One-click standard orthographic and oblique projection cameras

_This add-on is part of the presentation [The Missing Camera or: How I Learned to Stop Worrying and Love Oblique Projection](https://video.blender.org/w/porKQZAqcyYTLWctLNtyYQ) at BCON24._

## Install in Blender (recommended)

+ Open the Preference Menu: **Edit > Preferences**.
+ In **Get Extensions**, enter "pohlke" in the search box.
+ Click **Install**.

### or: from Blender Extensions Website

+ On the official extensions website at [extensions.blender.org/add-ons/pohlke/](https://extensions.blender.org/add-ons/pohlke/) click **Get Add-on**,
+ Drag'n'drop into Blender.
+ Follow the instructions to install.

## Usage
+ Place cursor.
+ Select the desired projection.
  + From the 'Add Cemera Menu' ('Shift + A')
  + In the 'Parallel Cams' tool menu ('N' shortcut).
+ A camera is created.
+ Change projection types and parameters in the 'Redo Panel'
+ Modify orthographic scale in the properties camera tab.

## How it works
+ Orthographic projections are determined by the camera's position and rotation angle.
+ For the oblique projections the pixel ratio is set automatically prior to rendering.

## Showcase

![oblique projections](docs/media/oblique.png)
![orthographic projections](docs/media/orthographic.png)

## Name

The add-on is named after Karl Wilhelm Pohlke, author of a generalizing parallel
projection theorem in 1853.

## Citation

A paper has not yet been published on the addon, but if you use it in your
academic work you can cite it from Zenodo:

Rippinger, J., & Lefèvre, M. (2024). Pohlke. Zenodo. [doi.org/10.5281/zenodo.7330245](https://doi.org/10.5281/zenodo.7330245)

## Contributors

+ [pioverfour](https://codeberg.org/pioverfour)

## License

The add-on is licensed under the GNU General Public License v3.0 or later. The full repository is [reuse compliant](https://api.reuse.software/info/codeberg.org/AlICe_lab/pohlke).
