# SPDX-FileCopyrightText: 2021-2024 Julien Rippinger, Michel Lefèvre <alicelab.be>
#
# SPDX-License-Identifier: GPL-3.0-or-later

import bpy
from bpy.app.handlers import persistent
from math import pi, radians, sin, asin, sqrt


CAMERA_SETTINGS = {
    "ISOMETRIC": {
        "name": "Isometric",
        "rotation": pi/4,
        "altitude": pi/2 - asin(sqrt(2)/sqrt(3)),  # the altitude of a cube's diagonal
        "flattening": 0,
    },
    "DIMETRIC": {
        "name": "Dimetric",
        "rotation": radians(23),
        "altitude": pi/6,
        "flattening": 0,
    },
    "TRIMETRIC": {
        "name": "Trimetric",
        "rotation": radians(34),
        "altitude": radians(23),
        "flattening": 0,
    },
    "MILITARY": {
        "name": "Military",
        "rotation": pi/6,
        "altitude": pi/4,
        "flattening": 1,
    },
    "MILITARY_SHORT": {
        "name": "Shortened Military",
        "rotation": pi/6,
        "altitude": pi/2 - (pi/4 * 0.82),
        "flattening": 1,
    },
    "CAVALIER": {
        "name": "Cavalier",
        "rotation": pi/4,
        "altitude": pi/4,
        "flattening": 1,
    },
    # "CAVALIER_ANGLE": {
    #     "name": "obl.cavalier.angle",
    #     "rotation": radians(30),
    #     "altitude": radians(15),
    #     "flattening": 1 / cos(radians(15)),
    # },
    "CABINET": {
        "name": "Cabinet",
        "rotation": pi/4,
        "altitude": pi/2 - (pi/4 * 0.82),
        "flattening": 1,
    },
}


def type_update(self, context):
    settings = CAMERA_SETTINGS[self.camera_type]
    self.rotation = settings["rotation"]
    self.altitude = settings["altitude"]
    self.flattening = settings["flattening"]


class Pohlke_OT_CreateCam(bpy.types.Operator):
    bl_idname = "view3d.create_parallel_camera"
    bl_label = "Create Parallel Camera"
    bl_description = "Create a camera using a parallel projection"
    bl_options = {'REGISTER', 'UNDO'}

    camera_type: bpy.props.EnumProperty(
        name="Camera Type",
        description="Type of projection for the new camera",
        items=(
            ("ISOMETRIC", "Isometric", "Create an isometric camera"),
            ("DIMETRIC", "Dimetric", "Create an dimetric camera"),
            ("TRIMETRIC", "Trimetric", "Create an trimetric camera"),
            ("MILITARY", "Military", "Create a military projection camera"),
            ("MILITARY_SHORT", "Military Shortened", "Create a shortened military projection camera"),
            ("CAVALIER", "Cavalier", "Create a cavalier projection camera"),
            ("CABINET", "Cabinet", "Create a cabinet projection camera"),
        ),
        default="ISOMETRIC",
        update=type_update,
    )
    rotation: bpy.props.FloatProperty(
        name="Rotation",
        description="Rotation of the camera around the vertical axis",
        default=pi/4.0,
        subtype='ANGLE',
    )
    altitude: bpy.props.FloatProperty(
        name="Altitude",
        description="Altitude from the side. 0° is top view, 90° is side view",
        default=radians(54.74),
        min=-pi/2.0,
        max=pi/2.0,
        subtype='ANGLE',
    )
    flattening: bpy.props.FloatProperty(
        name="Flattening",
        description=("How much to flatten the horizontal plane.\n"
                     "1 to keep a square, square.\n"
                     "0 to keep the aspect ratio as is"),
        soft_min=0.0,
        soft_max=5.0,
        default=1.0,
    )
    distance: bpy.props.FloatProperty(
        name="Distance",
        description="Distance from the 3D cursor to add the camera at",
        default=20.0,
        subtype='DISTANCE',
    )

    def execute(self, context):
        settings = CAMERA_SETTINGS[self.camera_type]
        scn = context.scene
        bpy.ops.object.camera_add()
        obj = context.object

        obj.data.type = "ORTHO"
        obj.data.ortho_scale = 10
        obj.name = settings["name"] + " Camera"
        obj.rotation_euler.x = pi/2.0 - self.altitude
        obj.rotation_euler.z = self.rotation
        obj.location += self.distance * obj.rotation_euler.to_matrix().inverted()[2]

        if self.altitude == 0.0:
            pixel_ratio = 1.0
        else:
            pixel_ratio = self.flattening / sin(abs(self.altitude))
        obj["pixel_ratio"] = pixel_ratio
        scn.render.pixel_aspect_x = pixel_ratio
        scn.render.pixel_aspect_y = 1.0

        bpy.ops.view3d.object_as_camera()
        return {"FINISHED"}


# UI


def draw_orthographic(layout):
    layout.label(text="Orthographic")
    layout.operator("view3d.create_parallel_camera", text="Isometric").camera_type = "ISOMETRIC"
    layout.operator("view3d.create_parallel_camera", text="Dimetric").camera_type = "DIMETRIC"
    layout.operator("view3d.create_parallel_camera", text="Trimetric").camera_type = "TRIMETRIC"


def draw_oblique(layout):
    layout.label(text="Oblique")
    layout.operator("view3d.create_parallel_camera", text="Military").camera_type = "MILITARY"
    layout.operator("view3d.create_parallel_camera", text="Military Shortened").camera_type = "MILITARY_SHORT"
    layout.operator("view3d.create_parallel_camera", text="Cavalier").camera_type = "CAVALIER"
    layout.operator("view3d.create_parallel_camera", text="Cabinet").camera_type = "CABINET"


class Pohlke_PT_projectionspanel(bpy.types.Panel):
    bl_idname = "POHLKE_PT_Camera_Panel"
    bl_label = "Add Cameras"
    bl_category = "Parallel Cams"
    bl_space_type = "VIEW_3D"
    bl_region_type = "UI"

    def draw(self, context):
        layout = self.layout

        draw_orthographic(layout)
        draw_oblique(layout)


class POHLKE_MT_camera_add_menu(bpy.types.Menu):
    bl_idname = "POHLKE_MT_camera_add_menu"
    bl_label = "Parallel"
    bl_options = {'SEARCH_ON_KEY_PRESS'}

    def draw(self, context):
        layout = self.layout

        draw_orthographic(layout)
        layout.separator()
        draw_oblique(layout)


def add_menu_func(self, context):
    layout = self.layout
    layout.separator()
    layout.menu("POHLKE_MT_camera_add_menu", text="Parallel", icon="VIEW_ORTHO")


@persistent
def set_pixel_ratio(scene):
    """
    change pixel ratio automatically on scene update
    """
    camera = scene.camera
    if camera is not None and "pixel_ratio" in camera:
        scene.render.pixel_aspect_x = camera["pixel_ratio"]
        scene.render.pixel_aspect_y = 1.0


# register addon
bl_info = {
    "name": "Pohlke",
    "description": "One-click standard orthographic and oblique projection cameras",
    "author": "Julien Rippinger, Michel Lefèvre, alicelab.be",
    "version": (1, 2, 0),
    "blender": (4, 2, 0),
    "location": "3D Viewport > Sidebar (N) > Parallel Cams",
    "warning": "",
    "doc_url": "https://codeberg.org/alice_lab/pohlke",
    "tracker_url": "https://codeberg.org/alice_lab/pohlke/issues",
    "category": "Camera",
}


classes = (
    Pohlke_OT_CreateCam,
    Pohlke_PT_projectionspanel,
    POHLKE_MT_camera_add_menu,
)

rgstr, unrgstr = bpy.utils.register_classes_factory(classes)


def register():
    rgstr()
    bpy.types.VIEW3D_MT_camera_add.append(add_menu_func)
    bpy.app.handlers.depsgraph_update_pre.append(set_pixel_ratio)
    bpy.app.handlers.frame_change_pre.append(set_pixel_ratio)

def unregister():
    unrgstr()
    bpy.types.VIEW3D_MT_camera_add.remove(add_menu_func)
    bpy.app.handlers.depsgraph_update_pre.remove(set_pixel_ratio)
    bpy.app.handlers.frame_change_pre.remove(set_pixel_ratio)


if __name__ == "__main__":
    register()
